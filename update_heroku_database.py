# Tool for updating production database with data from the local sqlite3 database

import psycopg2
import sqlite3

# Make sure the following creadentials are up-to-date 

DB_HOST = ""
DB_NAME = ""
DB_USER = ""
DB_PORT = 5432
DB_PASSWORD = ""

def transfer_foodtype(local_cur, remote_cur):
	local_cur.execute('SELECT id, food_type_name FROM post_foodtype')
	remote_cur.executemany('INSERT INTO post_foodtype(id, food_type_name) VALUES (%s, %s)', local_cur.fetchall())

def transfer_training(local_cur, remote_cur):
	local_cur.execute('SELECT  id, name, calories_per_hour FROM post_training')
	remote_cur.executemany('INSERT INTO post_training(id, name, calories_per_hour) VALUES (%s,%s,%s)', local_cur.fetchall())

def transfer_food(local_cur, remote_cur):
	# local_cur.execute('SELECT id, food_type_name FROM post_foodtype')
	# remote_cur.execute('SELECT id, food_type_name FROM post_foodtype')
	# local_types = local_cur.fetchall()
	# remote_types = remote_cur.fetchall()
	# ld = {}
	# for tid,name in local_types:
	# 	ld[name] = tid
	# rd = {}
	# for tid,name in remote_types:
	# 	rd[name] = tid
	# type_id_mappings = {}
	# for key in ld.keys():
	# 	type_id_mappings[ld[key]] = rd[key]

	local_cur.execute("""SELECT id,
	food_name,
	food_type_id,
	energy,
	fat,
	saturated,
	carb,
	sugar,
	protein,
	salt,
	def_weight,
	popularity,
	notes FROM post_food""")
	# local_foods = []
	# for food in local_cur.fetchall():
	# 	food = list(food)
	# 	food[1] = type_id_mappings[food[1]]
	# 	local_foods.append(food)

	remote_cur.executemany("""INSERT INTO post_food(id,
	food_name,
	food_type_id,
	energy,
	fat,
	saturated,
	carb,
	sugar,
	protein,
	salt,
	def_weight,
	popularity,
	notes) VALUES 
	(
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s
	)""", local_cur.fetchall())

remote_conn = None
local_conn = None

try:
	remote_conn = psycopg2.connect(
	host=DB_HOST,
	database=DB_NAME,
	user=DB_USER,
	password=DB_PASSWORD,
	port=DB_PORT)

	local_conn = sqlite3.connect('db.sqlite3')
# create a cursor
	remote_cur = remote_conn.cursor()
	local_cur = local_conn.cursor()
	
# execute a statement
	print('PostgreSQL database version:')
	remote_cur.execute('SELECT version()')
	db_vesrion = remote_cur.fetchone()
	print(db_vesrion)

	# transfer_foodtype(local_cur, remote_cur)
	transfer_training(local_cur, remote_cur)
	transfer_food(local_cur, remote_cur)
	remote_conn.commit()
	
except (Exception, psycopg2.DatabaseError) as error:
	print(error)
finally:
	if remote_conn is not None:
		remote_conn.close()
		print("PostgreSQL database connection closed.")
	if local_conn is not None:
		local_conn.close()
		print("SQLite3 database connection closed.")