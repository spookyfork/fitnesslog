from django.contrib import admin
from .models import FoodType, Food, Meal, BodyStatistics, Training, Exercise, Goals, Limits

admin.site.register(FoodType)
admin.site.register(BodyStatistics)
admin.site.register(Training)
admin.site.register(Exercise)
admin.site.register(Goals)
admin.site.register(Limits)


class FoodAdmin(admin.ModelAdmin):
    search_fields = ['food_name']


class MealAdmin(admin.ModelAdmin):
    list_display = ['id', 'food', 'date']


admin.site.register(Food, FoodAdmin)
admin.site.register(Meal, MealAdmin)
