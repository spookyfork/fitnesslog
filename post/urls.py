from django.urls import path
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.csrf import requires_csrf_token
from django.shortcuts import reverse
from . import views

app_name = "post"
urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('food/', views.new_food, name='new_food'),
    path('food/<int:pk>', views.new_food, name='food'),
    path('food/<int:pk>', views.new_food, name='edit_food'),
    path('foodlist/', login_required(views.FoodListView.as_view(), login_url='/login'), name='food_list'),

    path('meal/', views.meal, name='new_meal'),
    path('meal/<int:pk>', views.meal, name='meal'),
    path('meal/<int:pk>', views.meal, name='edit_meal'),
    path('meallist/', login_required(views.MealListView.as_view(), login_url='/login'),  name='meal_list'),
    path('delete_meal/<int:pk>', views.delete_meal, name='delete_meal'),

    path('weight/', views.weight, name='weight'),

    path('training/', views.training, name='new_training'),
    path('training/<int:pk>', views.training, name='training'),
    path('training/<int:pk>', views.training, name='edit_training'),
    path('training_list/', login_required(views.TrainingListView.as_view(), login_url='/login'), name='training_list'),

    path('exercise/', views.exercise, name='new_exercise'),
    path('exercise/<int:pk>', views.exercise, name='exercise'),
    path('exercise/<int:pk>', views.exercise, name='edit_exercise'),
    path('exercise_list/', login_required(views.ExerciseListView.as_view(), login_url='/login'), name='exercise_list'),
    path('delete_exercise/<int:pk>', views.delete_exercise, name='delete_exercise'),

    path('stats/', views.stats_view, name='stats'),
    path('stats/weight_graph', views.generate_weight_graph, name='weight_graph'),
    path('stats/calorie_graph', views.generate_calorie_graph, name='calorie_graph'),
    path('stats/nutrition_graph', views.generate_nutrition_graph, name='nutrition_graph'),
]