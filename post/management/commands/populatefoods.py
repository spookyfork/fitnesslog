from django.core.management.base import BaseCommand, CommandError

from post.models import Food, FoodType
import json


class Command(BaseCommand):
    help = 'Creates database based on json files'

    def add_arguments(self, parser):
        parser.add_argument('filename', nargs="+", type=str)

    def handle(self, *args, **options):
        for filename in options['filename']:
            foods = json.load(open(filename))
            for food in foods:
                note_string = "Wygenerowane automatycznie na podstawie http://www.ilewazy.pl "
                # Check what
                for k, v in food['nutrition'].items():
                    if v == 'b.d.':
                        note_string += k + ": " + v + "\n"
                        food['nutrition'][k] = 0
                portion_weight = float(food['portion_weight'])
                f = Food(
                    food_name=food['name'],
                    food_type=FoodType.objects.get(food_type_name=food['type']),
                    energy=int(food['nutrition']['Energia']) * 100 / portion_weight,
                    fat=round(float(food['nutrition']['Tłuszcz']) * 100 / portion_weight, 2),
                    # saturated=int(food['nutrition']['Energia']),
                    carb=round(float(food['nutrition']['Węglowodany']) * 100 / portion_weight, 2),
                    sugar=round(float(food['nutrition']['Cukry proste']) * 100 / portion_weight, 2),
                    protein=round(float(food['nutrition']['Białko']) * 100 / portion_weight, 2),
                    salt=round(float(food['nutrition']['Sól']) * 100 / portion_weight, 2),
                    def_weight=portion_weight,
                    notes=note_string
                )
                f.save()
