from bs4 import BeautifulSoup
import requests
import json
import time

# from postsite.post.models import Food, FoodType

# param for 100g values
p100g = "/rm/default/w/wart/"

"""
def pack_foods(names, kcals=None, proteins=None, fats=None, carbs=None, fibers=None, defweight=None):
    foods = []
    for i in range(len(names)):
        food = {
            'name': names[i],
            'kcal': kcals[i] if kcals else None,
            'kj': None,
            'carbs': carbs[i] if carbs else None,
            'proteins': proteins[i] if proteins else None,
            'fats': fats[i] if fats else None,
            'fiber': fibers[i] if fibers else None,
            'defaultweight': defweight[i] if defweight else '100'
        }
        foods.append(food)
    return foods


def get_def_weight(product, page=1):
    r = requests.get("http://www.ilewazy.pl/" + 'produkty/page/' + str(page) + '/q/' + str(product))
    if r.status_code == 200:
        hxs = Selector(r)
        # titles = hxs.xpath('//a[@class="thumb-subtitle"]/text()').extract()
        def_weights = hxs.xpath('//span[@class="product-thumbnail-weight"]/text()').extract()
        def_weights = [v.replace('g', '').strip() for v in def_weights]
        return def_weights


def get_names(r):
    hxs = Selector(r)
    titles = hxs.xpath('//a[@class="thumb-subtitle"]/text()').extract()
    ntitles = [title.strip() for title in titles]
    return ntitles


def get_kcals(r):
    hxs = Selector(r)
    kcals = hxs.xpath('//li[@title="Energia"]/strong/text()').extract()
    kcals = [v.replace('kcal', '').strip() for v in kcals]
    clearbd(kcals)
    return kcals


def get_proteins(r):
    hxs = Selector(r)
    proteins = hxs.xpath('//li[@title="Białko"]/text()').extract()
    proteins = [v.replace('B:', '').replace('g', '').strip().replace(',', '.') for v in proteins]
    clearbd(proteins)
    return proteins


def get_fats(r):
    hxs = Selector(r)
    fats = hxs.xpath('//li[@title="Tłuszcz"]/text()').extract()
    fats = [v.replace('T:', '').replace('g', '').strip().replace(',', '.') for v in fats]
    clearbd(fats)
    return fats


def get_carbs(r):
    hxs = Selector(r)
    carbs = hxs.xpath('//li[@title="Węglowodany"]/text()').extract()
    carbs = [v.replace('W:', '').replace('g', '').strip().replace(',', '.') for v in carbs]
    clearbd(carbs)
    return carbs


def get_fibers(r):
    hxs = Selector(r)
    fibers = hxs.xpath('//li[@title="Błonnik"]/text()').extract()
    fibers = [v.replace('Bł:', '').replace('g', '').strip().replace(',', '.') for v in fibers]

    return fibers


# get all products from ilewazy.pl
def find(product):
    links = ['produkty/page/1/q/' + str(product)]
    page = 1
    names = []
    kcals = []
    proteins = []
    fats = []
    carbs = []
    fibers = []
    defw = []
    while len(links) > 0:
        r = requests.get("http://www.ilewazy.pl/" + links[0] + p100g)
        if r.status_code != 200:
            raise RuntimeError("Incorrect status code! " + str(r.status_code))
        names += get_names(r)
        kcals += get_kcals(r)
        proteins += get_proteins(r)
        fats += get_fats(r)
        carbs += get_carbs(r)
        fibers += get_fibers(r)
        defw += get_def_weight(product, page)
        hxs = Selector(r)
        page += 1
        links = hxs.xpath('//a[@data-page="' + str(page) + '"]/@href').extract()

    foods = pack_foods(names, kcals, proteins, fats, carbs, fibers, defw)
    return foods

"""
host_address = "http://www.ilewazy.pl"

types_mapping = {
    'Bakalie i nasiona': 'Bakalie i nasiona',
    'Mięso, wędliny i dania mięsne': 'Mięso, wędliny',
    'Nabiał i jaja': 'Nabiał, jaja',
    'Napoje': 'Napoje',
    'Owoce i ich przetwory': 'Owoce',
    'Produkty gotowe': 'Produkty gotowe',
    'Produkty zbożowe': 'Produkty zbożowe',
    'Ryby i dania rybne': 'Ryby',
    'Słodycze i przekąski': 'Słodycze i przekąski',
    'Tłuszcze': 'Tłuszcze',
    'Warzywa i ich przetwory': 'Warzywa',
    # 'Wykonane przez Izabelę': '/kategoria/wykonane-przez-izabele'
}

nutritional_mapping = {
    'Energia': 'energy',
    'Białko': 'protein',
    'Tłuszcz': 'fat',
    'Węglowodany': 'carb',
    'Cukry proste': 'sugar',
    # 'Błonnik': 'Produkty zbożowe',
    'Sól': 'salt',
}

DELAY = 10


def sleep(seconds):
    time.sleep(seconds)


def get_food_types_links(soup: BeautifulSoup):
    ul = soup.select("#header > div > div.navbar > div > div.nav-collapse.collapse > ul > li:nth-child(1) > ul")
    types_and_links = {}
    for li in ul[0].find_all('li'):
        types_and_links[li.a.string] = li.a['href']
    return types_and_links


foods = []


def parse_page(food_type_name, soup):
    thumbnails = soup.find_all('li', class_='span3')
    for t in thumbnails:
        if t.a is None:
            # this is an ad
            continue
        else:
            name = t.find("div", class_='subtitle').a.text.strip()
            weight_string = t.find("div", class_='product-params').find('span',
                                                                        class_='product-thumbnail-weight').text.replace(
                'g', '').strip()
            nutr = {}
            prefixes = ["B: ", "T: ", "W: ", "C: ", "Bł: ", "S: "]
            for li in t.find("ul").find_all('li'):
                # can be 'b.d.',
                text = li.text
                if text.find('b.d.') != -1:
                    # brak danych
                    nutr[li['title']] = 'b.d.'
                else:
                    for pre in prefixes:
                        text = text.replace(pre, '')
                    text = text.replace('kcal', '').replace('g', '').strip().replace(',', '.')
                    nutr[li['title']] = text
            foods.append({"name": name, "nutrition": nutr, 'type': food_type_name, 'portion_weight': weight_string})


# Pass the first page to this function
def get_all_in_type(type, url):
    while True:
        print("Getting page " + url)
        # Download page
        r = requests.get(url)
        if r.status_code != 200:
            print("Error on:" + url + "--" + str(r.status_code))
            exit(1)
        soup = BeautifulSoup(r.text, 'html.parser')
        # Parse it
        parse_page(type, soup)
        # Check if there is next page button
        ul = soup.select_one("#ilewazy_wyszukiwarka > div > div.row.pager-search > div.span5 > div > ul")
        last_li = ul.find_all('li')[-1]
        if last_li.a is None:
            # They use <span> if there is no next page with <a>
            break
        else:
            url = host_address + last_li.a['href']
            # Wait the defined delay time
            sleep(DELAY)


if __name__ == "__main__":
    # Get website
    response = requests.get(host_address)
    # Check status code
    if response.status_code != 200:
        print(response.status_code)
    else:
        soup = BeautifulSoup(response.text, 'html.parser')
        for k, v in get_food_types_links(soup).items():
            if types_mapping.get(k):
                get_all_in_type(types_mapping.get(k), host_address + v)
    json.dump(foods, open("ilewazy_foods.json", 'w'))
