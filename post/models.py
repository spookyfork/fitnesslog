import datetime

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


class FoodType(models.Model):
    food_type_name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.food_type_name


def validate_value(value):
    if value < 0:
        raise ValidationError(_('%(value)s is negative'), params={'value': value}, )


class BodyStatistics(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    weight = models.FloatField(default=70)
    date = models.DateField(default=timezone.now)

    # Date should be unique (for each user)

    def __str__(self):
        return "Weight: " + str(self.weight) + " " + str(self.date)


class Food(models.Model):
    food_name = models.CharField(max_length=200)
    food_type = models.ForeignKey(FoodType, on_delete=models.CASCADE)
    energy = models.IntegerField(default=0, validators=[validate_value])
    fat = models.FloatField(default=0.0, validators=[validate_value])
    saturated = models.FloatField(default=0.0, validators=[validate_value])
    carb = models.FloatField(default=0.0, validators=[validate_value])
    sugar = models.FloatField(default=0.0, validators=[validate_value])
    protein = models.FloatField(default=0.0, validators=[validate_value])
    salt = models.FloatField(default=0.0, validators=[validate_value])
    def_weight = models.FloatField(default=100, validators=[validate_value])

    popularity = models.IntegerField(default=0)
    notes = models.CharField(max_length=2048, null=True, blank=True)

    def __str__(self):
        return self.food_name


class Meal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    food = models.ForeignKey(Food, on_delete=models.PROTECT)
    date = models.DateTimeField(default=timezone.now)
    weight = models.FloatField(default=0, validators=[validate_value])

    def __str__(self):
        return self.food.food_name + " " + str(self.date)


class Training(models.Model):
    name = models.CharField(max_length=200)
    calories_per_hour = models.FloatField(default=0)

    def __str__(self):
        return self.name


class Exercise(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    training = models.ForeignKey(Training, on_delete=models.SET_NULL, null=True)
    minutes = models.IntegerField(default=60)
    date = models.DateTimeField(default=timezone.now)
    burned_calories_custom = models.FloatField(default=None, null=True, blank=True)

    def __str__(self):
        return self.training.name + str(self.date)


def now_plus_one_day():
    return timezone.now() + datetime.timedelta(days=1)


class Schedule(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    date_start = models.DateTimeField(default=timezone.now)
    date_end = models.DateTimeField(default=now_plus_one_day)
    message = models.CharField(max_length=1024)

    def __str__(self):
        return self.name


# For informing/highlighting elements when the user is close to reaching limit
# Also can be used when creating statistics
class Limits(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    time_span = models.PositiveIntegerField(default=1)

    # FIXME setting blank and null to True allows us to save empty fields in admin view
    # but it does not work with TextField and CharField
    energy = models.FloatField(default=None, null=True, blank=True)
    fat = models.FloatField(default=None, null=True, blank=True)
    saturated = models.FloatField(default=None, null=True, blank=True)
    carb = models.FloatField(default=None, null=True, blank=True)
    sugar = models.FloatField(default=None, null=True, blank=True)
    salt = models.FloatField(default=None, null=True, blank=True)
    weight = models.FloatField(default=None, null=True, blank=True)

    def __str__(self):
        s = self.user.username + " (time_span: " + str(self.time_span) + ") - "
        for k in ["energy", "fat", "saturated", "carb", "sugar", "salt", "weight"]:
            if self.__getattribute__(k) is not None:
                s += k + ":" + str(self.__getattribute__(k)) + " "
        return s

    class Meta:
        unique_together = (('user', 'time_span'),)


class Goals(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    deadline = models.DateField()
    weight = models.FloatField(default=70)

    # TODO add support for other goals (calorie intake?, sugars?)
    # maybe create a new table for the goalTypes

    def __str__(self):
        s = self.user.username + " (deadline: " + str(self.deadline) + ") - "
        if self.weight is not None:
            s += "Weight:" + str(self.weight)
        return s
