import io
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import timedelta, datetime
from django.core.exceptions import PermissionDenied
from django.db.models import Count
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, get_object_or_404, reverse
from django.views import generic
from django.http import HttpRequest, HttpResponseRedirect, Http404, FileResponse
from django.utils import timezone, dateformat, dateparse

from .models import Meal, Food, FoodType, BodyStatistics, Training, Exercise, Goals, Limits

matplotlib.use('agg')


def login_view(request):
    if request.method == "GET":
        return render(request, 'post/login.html')
    elif request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        print(request.POST['csrfmiddlewaretoken'])
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(request.GET['next'] if request.GET.get('next') else reverse("post:index"))
        else:
            render(request, 'post/login.html', {'error': "Niepoprawne dane."})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse("post:login"))


def create_stats(user, date):
    today = date.replace(hour=0, minute=0, second=0, microsecond=0)
    meals = Meal.objects.filter(user=user, date__gte=today, date__lt=today + timedelta(days=1))
    stats = {
        "fat": 0,
        "saturated": 0,
        "carb": 0,
        "sugar": 0,
        "salt": 0,
        "weight": 0,
    }
    for m in meals:
        stats['fat'] += m.food.fat * m.weight / 100.0
        stats['saturated'] += m.food.saturated * m.weight / 100.0
        stats['carb'] += m.food.carb * m.weight / 100.0
        stats['sugar'] += m.food.sugar * m.weight / 100.0
        stats['salt'] += m.food.salt * m.weight / 100.0
        stats['weight'] += m.weight

    stats['fat'] = round(stats['fat'], 1)
    stats['saturated'] = round(stats['saturated'], 1)
    stats['carb'] = round(stats['carb'], 1)
    stats['sugar'] = round(stats['sugar'], 1)
    stats['salt'] = round(stats['salt'], 1)
    stats['weight'] = round(stats['weight'], 1)
    return stats


def get_meals(user, date):
    today = date.replace(hour=0, minute=0, second=0, microsecond=0)
    tomorrow = today + timedelta(days=1)
    meals = Meal.objects.filter(user=user, date__gte=today, date__lt=tomorrow)
    return meals


def get_weight(user, date):
    try:
        bs = BodyStatistics.objects.get(user=user, date=date)
    except (KeyError, BodyStatistics.DoesNotExist):
        return None
    else:
        return bs.weight


def get_excercises(user, date):
    today = date.replace(hour=0, minute=0, second=0, microsecond=0)
    tomorrow = today + timedelta(days=1)
    return Exercise.objects.filter(user=user, date__gte=today, date__lt=tomorrow)


def index(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))

    current_day = request.GET.get('date')
    if current_day is None:
        current_day = timezone.now()
    else:
        current_day = datetime.strptime(current_day, "%Y-%m-%d")
        

    # Add calories attribute
    kcal_sum = 0
    meals = get_meals(request.user, current_day)
    for m in meals:
        m.calories = m.weight * m.food.energy / 100
        kcal_sum += m.calories
    stats = create_stats(request.user, current_day)
    weight = get_weight(request.user, current_day)
    favourite_foods = Meal.objects.filter(user=request.user).values('food').annotate(total=Count('food')).order_by(
        '-total')[
                      :5]
    for r in favourite_foods:
        r['food'] = Food.objects.get(pk=r['food'])
    exercises = get_excercises(request.user, current_day)


    return render(request, "post/index.html",
                  {'meals': meals,
                   'sum': round(kcal_sum),
                   'fill': range(10 - len(meals)),
                   'stats': stats,
                   'weight': weight,
                   'popular_foods': Food.objects.filter(popularity__gte=1).order_by('-popularity')[:5],
                   'favourite_foods': favourite_foods,
                   'exercises': exercises,
                   'exercises_energy_sum': sum(
                       [e.minutes * e.training.calories_per_hour / 60 for e in exercises]),
                   'exercises_time_sum': sum(
                       [e.minutes for e in exercises]),
                   'limits': Limits.objects.filter(user=request.user, time_span=1).first(),
                   'goals': Goals.objects.filter(user=request.user, deadline__gte=current_day.date()).first(),
                   'current_day': current_day,
                   'previous_day': current_day + timedelta(days=-1),
                   'next_day': current_day + timedelta(days=1),
                   })


def weight(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))
    if request.method == "POST":
        date = timezone.now().date()
        weight_ = float(request.POST["weight"])
        user = request.user
        bs = None
        try:
            bs = BodyStatistics.objects.get(user=user, date=date)
        except (KeyError, BodyStatistics.DoesNotExist):
            pass
        if bs is None:
            BodyStatistics(user=user, date=date, weight=weight_).save()
        else:
            bs.weight = weight_
            bs.save()
        return HttpResponseRedirect(request.GET['next'] if request.GET.get('next') else reverse('post:index'))
    else:
        raise Http404()


def new_food(request: HttpRequest, pk=None):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))
    if request.method == "POST":
        if pk is None:
            f = Food()
        else:
            f = get_object_or_404(Food, pk=pk)
        f.food_name = request.POST['fname']
        f.food_type = get_object_or_404(FoodType, pk=request.POST['ftype'])
        f.energy = request.POST['fenergy']
        f.fat = request.POST['ffat']
        f.saturated = request.POST['fsaturated']
        f.carb = request.POST['fcarb']
        f.sugar = request.POST['fsugar']
        f.protein = request.POST['fprotein']
        f.salt = request.POST['fsalt']
        f.def_weight = request.POST['fdef_weight']
        f.save()
        return HttpResponseRedirect(request.GET['next'] if request.GET.get('next') else reverse("post:food_list"))
    if request.method == "GET":
        if pk is None:
            return render(request, "post/food.html",
                          {'food_type': FoodType.objects.all()
                           })
        else:
            f = get_object_or_404(Food, pk=pk)
            return render(request, "post/food.html",
                          {'food_type': FoodType.objects.all(),
                           'food': f,
                           })


def meal(request: HttpRequest, pk=None):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))
    if request.method == "POST":
        if pk is None:
            m = Meal()
        else:
            m = get_object_or_404(Meal, user=request.user, pk=pk)
        m.user = request.user
        m.food = get_object_or_404(Food, pk=request.POST['mfood'])
        m.date = request.POST['mdate']
        if request.POST['weightchoice'] == '1':
            m.weight = m.food.def_weight * int(request.POST['mportions'])
        else:
            m.weight = request.POST['mweight']
        m.save()
        m.food.popularity += 1
        m.food.save()
        return HttpResponseRedirect(request.GET['next'] if request.GET.get('next') else reverse("post:meal_list"))
    if request.method == "GET":
        food_list = Food.objects.all()
        if pk is None:
            selected_food = None
            if request.GET.get('food'):
                selected_food = get_object_or_404(Food, pk=request.GET.get('food'))
            return render(request, "post/meal.html",
                          {"food_list": food_list,
                           'selected_food': selected_food,
                           'date': dateformat.format(timezone.now(), 'Y-m-d'),
                           })
        else:
            m = get_object_or_404(Meal, user=request.user, pk=pk)
            return render(request, "post/meal.html",
                          {"meal": m,
                           "food_list": food_list})


def delete_meal(request, pk):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))
    m = get_object_or_404(Meal, user=request.user, pk=pk)
    if request.user != m.user:
        raise PermissionDenied("Nie twój posiłek.")
    m.delete()
    return HttpResponseRedirect(request.GET['next'] if request.GET.get('next') else reverse('post:index'))


class IndexView(generic.DetailView):
    template_name = "post/index.html"

    def get_queryset(self):
        pass


class FoodListView(generic.ListView):
    template_name = "post/food_list.html"
    context_object_name = 'food_list'
    paginate_by = 20
    login_required = True

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['food_type'] = FoodType.objects.all()
        return context

    def get_queryset(self):
        query = self.request.GET.get('q')
        order = self.request.GET.get('order')
        type_filter = self.request.GET.get('type_filter')

        foods = Food.objects.all()

        if query:
            foods = Food.objects.filter(food_name__icontains=query)
        if order:
            foods = foods.order_by(order)
        if type_filter and type_filter != '-1':
            foods = foods.filter(food_type = type_filter)
        return foods



class MealListView(generic.ListView):
    template_name = "post/meal_list.html"
    context_object_name = 'meal_list'

    def get_queryset(self):
        return Meal.objects.filter(user=self.request.user)


def training(request, pk=None):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))
    if request.method == "POST":
        if pk is None:
            # Creating a new one with data from the user
            new_training = Training(name=request.POST['name'], calories_per_hour=request.POST['calories'])
            new_training.save()
            return HttpResponseRedirect(request.GET['next'] if request.GET.get('next') else reverse('post:training_list'))
        else:
            # Editing an existing one with data from the user
            existing_training = get_object_or_404(Training, pk=pk)
            existing_training.name = request.POST['name']
            existing_training.calories_per_hour = request.POST['calories']
            existing_training.save()
            return HttpResponseRedirect(request.GET['next'] if request.GET.get('next') else reverse('post:training_list'))
    if request.method == "GET":
        if pk is None:
            # Sending a form for the new entry
            return render(request, 'post/training.html')
        else:
            # Sending a form for existing entry for editing
            t = get_object_or_404(Training, pk=pk)
            return render(request, 'post/training.html', {
                'training': t
            })


def exercise(request, pk=None):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))
    if request.method == "POST":
        if pk is None:
            # adding new
            train = Training.objects.get(pk=request.POST['training'])
            new_ex = Exercise(user=request.user, training=train,
                              minutes=request.POST['time'], date=request.POST['date'])
            new_ex.save()
            return HttpResponseRedirect(request.GET['next'] if request.GET.get('next') else reverse("post:exercise_list"))
        else:
            train = Training.objects.get(pk=request.POST['training'])
            existing_ex = Exercise.objects.get(user=request.user, pk=pk)
            existing_ex.training = train
            existing_ex.minutes = int(request.POST['time'])
            existing_ex.date = request.POST['date']
            existing_ex.save()
            return HttpResponseRedirect(request.GET['next'] if request.GET.get('next') else reverse("post:exercise_list"))
    if request.method == "GET":
        if pk is None:
            ex = None
        else:
            ex = get_object_or_404(Exercise, user=request.user, pk=pk)
        training_list = Training.objects.all()
        try:
            selected = Training.objects.get(pk=request.GET.get('t_id'))
        except Training.DoesNotExist:
            selected = None
        return render(request, 'post/exercise.html', {
            'exercise': ex,
            'training_list': training_list,
            'selected_training': selected,
        })


def delete_exercise(request, pk):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))
    ex = get_object_or_404(Exercise, user=request.user, pk=pk)
    ex.delete()
    return HttpResponseRedirect(request.GET['next'] if request.GET.get('next') else reverse("post:index"))


class TrainingListView(generic.ListView):
    template_name = 'post/training_list.html'
    context_object_name = 'training_list'

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            return Training.objects.filter(name__icontains=query)
        else:
            return Training.objects.all()


class ExerciseListView(generic.ListView):
    template_name = 'post/exercise_list.html'
    context_object_name = 'exercise_list'

    def get_queryset(self):
        return Exercise.objects.filter(user=self.request.user)


def get_start_end_dates(request):
    try:
        start_date = dateparse.parse_date(request.GET.get("start_date"))
        end_date = dateparse.parse_date(request.GET.get("end_date"))
    except TypeError:
        start_date = end_date = None
    if end_date is None:
        end_date = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
    if start_date is None:
        start_date = end_date + timedelta(days=-30)
    return start_date, end_date


def stats_view(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))
    start_date, end_date = get_start_end_dates(request)
    return render(request, "post/stats.html", {
        'start_date': start_date,
        'end_date': end_date,
    })


def generate_weight_graph(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))
    start_date, end_date = get_start_end_dates(request)
    x = [start_date + timedelta(days=d) for d in range((end_date - start_date).days + 1)]
    y = []
    for d in x:
        try:
            y.append(BodyStatistics.objects.get(user=request.user, date=d).weight)
        except BodyStatistics.DoesNotExist:
            y.append(None)
    # Masking missing data
    last_w = None
    last_i = 0
    for i, w in enumerate(y):
        if w is not None and (last_w is None or i - last_i == 1):
            last_i = i
            last_w = w
            continue

        if w is not None and i - last_i > 1:
            for j in range(last_i + 1, i):
                y[j] = last_w + (w - last_w) / (i - last_i) * (j - last_i)
            last_w = w
            last_i = i
            continue
    fig, ax = plt.subplots()
    ax.set_xlabel("Czas")
    ax.set_ylabel("Waga [kg]")
    ax.set_title("Waga Ciała")
    ax.set_xlim(left=x[0], right=x[-1])
    ax.get_xaxis().set_major_locator(mdates.WeekdayLocator(interval=1))
    ax.get_xaxis().set_major_formatter(mdates.DateFormatter("%d %b"))
    ax.plot(x, y, 'o-')
    ax.grid(True)
    try:
        goal = Goals.objects.filter(user=request.user, deadline__gte=timezone.now().date()).first()
        h3 = ax.axhline(goal.weight, xmin=0.0, xmax=1.0, alpha =0.5, color='r', label='Cel', linestyle='--')
    except Limits.DoesNotExist:
        pass

    buf = io.BytesIO()
    fig.savefig(buf, format='png')
    plt.close(fig)
    buf.seek(0)
    return FileResponse(buf, filename='weight.png')


def generate_calorie_graph(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))
    start_date, end_date = get_start_end_dates(request)
    x = [start_date + timedelta(days=d) for d in range((end_date - start_date).days + 1)]
    consumed = []
    burned = []
    for d in x:
        try:
            consumed.append(sum([m.weight * m.food.energy / 100.0
                                 for m in Meal.objects.filter(user=request.user,
                                                              date__gte=d, date__lt=d + timedelta(days=1))]))
        except Meal.DoesNotExist:
            consumed.append(None)
        try:
            burned.append(sum([e.minutes * e.training.calories_per_hour / 60.0
                               for e in Exercise.objects.filter(user=request.user, date__gte=d,
                                                                date__lte=d + timedelta(days=1))]))
        except Exercise.DoesNotExist:
            burned.append(None)
    fig, ax = plt.subplots()
    ax.set_xlabel("Czas")
    ax.set_ylabel("Energia [kcal]")
    ax.set_title("Energia")
    h1, = ax.plot(x, consumed, label='Skonsumowana', color='red')
    h2, = ax.plot(x, burned, label='Spalona', color='green')
    ax.get_xaxis().set_major_locator(mdates.WeekdayLocator(interval=1))
    ax.get_xaxis().set_major_formatter(mdates.DateFormatter("%d %b"))
    
    ax.grid(True)
    try:
        limits = Limits.objects.get(user=request.user)
        h3 = ax.axhline(limits.energy, xmin=0.0, xmax=1.0, alpha =0.5, color='red', label='Limit energii', linestyle='--')
        ax.legend(handles=[h1, h2, h3], loc='upper left')
    except Limits.DoesNotExist:
            ax.legend(handles=[h1, h2], loc='upper left')
    
    buf = io.BytesIO()
    fig.savefig(buf, format='png')
    plt.close(fig)
    buf.seek(0)
    return FileResponse(buf, filename='calorie.png')


def generate_nutrition_graph(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect(reverse("post:login"))
    start_date, end_date = get_start_end_dates(request)
    x = [start_date + timedelta(days=d) for d in range((end_date - start_date).days + 1)]
    fat = []
    carb = []
    protein = []
    salt = []
    for d in x:
        try:
            meals = Meal.objects.filter(user=request.user, date__gte=d, date__lt=d + timedelta(days=1))
            fat.append(sum([m.weight * m.food.fat / 100.0 for m in meals]))
            carb.append(sum([m.weight * m.food.carb / 100.0 for m in meals]))
            protein.append(sum([m.weight * m.food.protein / 100.0 for m in meals]))
            salt.append(sum([m.weight * m.food.salt / 100.0 for m in meals]))
        except Meal.DoesNotExist:
            fat.append(None)
            carb.append(None)
            protein.append(None)
            salt.append(None)

    fig, ax = plt.subplots()
    ax.set_xlabel("Czas")
    ax.set_ylabel("Waga [g]")
    ax.set_title("Wartości odżywcze")
    h1, = ax.plot(x, fat, label='Tłuszcz', color='tab:blue')
    h2, = ax.plot(x, carb, label='Węglowodany', color='tab:orange')
    h3, = ax.plot(x, protein, label='Białko', color="tab:green")
    h4, = ax.plot(x, salt, label='Sól', color='tab:purple')
    ax.get_xaxis().set_major_locator(mdates.WeekdayLocator(interval=1))
    ax.get_xaxis().set_major_formatter(mdates.DateFormatter("%d %b"))
    ax.grid(True)

    try:
        limits = Limits.objects.get(user=request.user)
        hs = []
        if limits.fat is not None:
            hs.append(ax.axhline(limits.fat, xmin=0.0, xmax=1.0, alpha =0.5, color='tab:blue', label='Limit tłuszczy', linestyle='--'))
        if limits.carb is not None:   
            hs.append(ax.axhline(limits.carb, xmin=0.0, xmax=1.0, alpha =0.5, color='tab:orange', label='Limit węglowodanów', linestyle='-.'))
        if limits.salt is not None:   
            hs.append(ax.axhline(limits.salt, xmin=0.0, xmax=1.0, alpha =0.5, color='tab:purple', label='Limit soli', linestyle='-.'))
        ax.legend(handles=[h1, h2, h3, h4, *hs], loc='upper left')
    except Limits.DoesNotExist:
            ax.legend(handles=[h1, h2, h3, h4], loc='upper left')
    buf = io.BytesIO()
    fig.savefig(buf, format='png')
    plt.close(fig)
    buf.seek(0)
    return FileResponse(buf, filename='nutrition.png')
